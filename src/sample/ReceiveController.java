package sample;

import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;

import java.net.URL;
import java.util.ResourceBundle;

public class ReceiveController implements Initializable, EventHandler<ActionEvent> {
    public ListView listView;
    public Label lblIp;
    public Label lblStatus;
    public Button btnBack;
    public Button btnCancel;
    public ProgressBar progressDummy;
    public Label lblDummy;

    private ReceiverSocket receiverSocket;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnBack.setOnAction(this);
        btnCancel.setOnAction(this);
        startReceiving();
    }



    @Override
    public void handle(ActionEvent event) {
        if(btnCancel == event.getSource())
            cancel();
        else
            back();
    }

    private void back() {
        System.out.println("Back pressed");

    }

    private void cancel() {
        System.out.println("Cancel pressed");
    }

    private void startReceiving(){
        receiverSocket = new ReceiverSocket();

        lblDummy.textProperty().bind(receiverSocket.messageProperty());
        progressDummy.progressProperty().bind(receiverSocket.progressProperty());

        receiverSocket.setOnRunning(event -> {

        });

        receiverSocket.setOnSucceeded(event -> {

        });

        receiverSocket.setOnFailed(event -> {

        });

        new Thread(receiverSocket).start();
    }
}
