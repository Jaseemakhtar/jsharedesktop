package sample;

import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class SendController implements Initializable {
    public ListView listView;
    public Button btnBack;
    public Button btnCancel;
    public TextField txtIpAddress;
    public Button btnConnect;
    public ProgressBar progressBar;
    public Label lblStatus;
    public Button btnSelect;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
