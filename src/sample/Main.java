package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application implements EventHandler<ActionEvent> {
    private Stage rootStage;
    private Button btnSend;
    private Button btnReceive;
    private Button btnBack;

    @Override
    public void start(Stage primaryStage){
        rootStage = primaryStage;
        loadHome();
        rootStage.setTitle("JShare");
        rootStage.show();
    }

    private void loadHome(){
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("sample.fxml"));
            btnReceive = (Button) root.lookup("#btnReceive");
            btnSend = (Button) root.lookup("#btnSend");

            btnSend.setOnAction(this);
            btnReceive.setOnAction(this);
            rootStage.setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startSend(){
        try {
            Parent sender = FXMLLoader.load(getClass().getResource("SendLayout.fxml"));
            btnBack = (Button) sender.lookup("#btnBack");
            btnBack.setOnAction(this);
            rootStage.setScene(new Scene(sender));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startReceive(){
        try {
            Parent sender = FXMLLoader.load(getClass().getResource("ReceiveLayout.fxml"));
            btnBack = (Button) sender.lookup("#btnBack");
            btnBack.setOnAction(this);
            rootStage.setScene(new Scene(sender));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void handle(ActionEvent event) {
        if(event.getSource() == btnSend){
            startSend();
        }else if(event.getSource() == btnReceive){
            startReceive();
        }else{
            loadHome();
        }
    }
}
