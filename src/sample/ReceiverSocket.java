package sample;

import javafx.concurrent.Task;

import java.io.*;
import java.net.*;

public class ReceiverSocket extends Task<String> {
    private ServerSocket serverSocket;
    private Socket client;
    private InputStream inputStream;
    private OutputStream outputStream;
    private FileOutputStream fileOutputStream;
    private File file;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;

    @Override
    protected String call(){
        try{
            serverSocket = new ServerSocket(7313);
            print("Server started...");

            updateMessage("Server started: " + serverSocket.getInetAddress().getHostAddress());

            client = serverSocket.accept();
            print("Client connected: " + client.getInetAddress().toString());
            updateMessage("Connected: " + client.getInetAddress().getHostAddress());
            inputStream = client.getInputStream();
            outputStream = client.getOutputStream();
            dataInputStream = new DataInputStream(inputStream);
            dataOutputStream = new DataOutputStream(outputStream);

            String message = dataInputStream.readUTF();
            String fileName;
            long fileSize;

            if(message.equals("receive")){
                    dataOutputStream.writeUTF("ok");
                    fileName = dataInputStream.readUTF();
                    fileSize = dataInputStream.readLong();
                    long fileSize1 = fileSize;
                    int fileLength = (int) (fileSize / 1024);

                    print("File Name: " + fileName);
                    print("File Size: " + fileSize);

                    updateMessage(fileName);

                    file = new File(fileName);
                    fileOutputStream = new FileOutputStream(file);

                    int read;
                    int total = 0;

                    byte[] bytes = new byte[1024 * 8];
                    while((read = inputStream.read(bytes)) > 0  && fileSize > 0){
                        fileOutputStream.write(bytes);
                        total += read;
                        fileSize -= read;
                        updateProgress(total, fileSize1);
                    }
                    fileOutputStream.flush();
                    dataOutputStream.writeUTF("success");
                    updateMessage("Success");
                    serverSocket.close();

            }

            return "Success";

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void print(Object msg){
        System.out.println(msg);
    }
}
